<a href="https://www.linkedin.com/in/jack-mcnulty-1017772b4/"><img src="https://img.shields.io/badge/-LinkedIn-0072b1?&style=for-the-badge&logo=linkedin&logoColor=white" /></a>
<a href="https://github.com/mcnlty"><img src="https://img.shields.io/badge/GitHub-100000?style=for-the-badge&logo=github&logoColor=white" /></a>
<a href="https://mcnulty.au/"><img src="https://img.shields.io/badge/website-000000?style=for-the-badge&logo=About.me&logoColor=white" /></a>
<a href="mailto:mcnlty@pm.me"><img src="https://img.shields.io/badge/ProtonMail-8B89CC?style=for-the-badge&logo=protonmail&logoColor=white" /></a>

# Hello, I'm Jack

[PGP Key](https://keys.openpgp.org/search?q=mcnlty%40pm.me)
## Current Projects
- My Home Lab/Selfhosting
- My Personal Website